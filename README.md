# Instructions

- Déposer un fichier du type `nom-prenom.txt` sur ce dépôt
- Le fichier doit contenir votre votre nom, votre prenom et votre email
- Un fichier par personne

Un example de fichier est [accessible ici](https://bitbucket.org/vjousse/l2_2013/src/8ba3e3d0da177c4c23e2f531083b0b6fcaf73762/jousse-vincent.txt?at=master).

Vous aurez certainement besoin des trois commandes suivantes :

- `git clone`
- `git add`
- `git commit`
- `git push`
